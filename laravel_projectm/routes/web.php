<?php

use App\Http\Controllers\Category;
use App\Http\Controllers\Test1;
use App\Http\Controllers\Test2;
use App\Http\Controllers\Test3;
use App\Http\Controllers\Test4;
use Illuminate\Support\Facades\Route;
use PHPUnit\Framework\Test;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {


//     return view('welcome');
// });
// Route::get('/test', [Category::class, 'index']);
// Route::get('/test1', [Test1::class, 'index1']);
// Route::get('/test2', [Test2::class, 'index2']);
// Route::get('/test3', [Test3::class, 'index3']);
// Route::get('/test4', [Test4::class, 'index4']);


Route::get('/welcome', function () {


    return view('welcome');
});

Route::get('/contact', function () {


    return view('Contact');
});


Route::get('/about', function () {


    return view('about');
})->name('about');


Route::get('/test', [Category::class, 'index'])->name('Category.index');
